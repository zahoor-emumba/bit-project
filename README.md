# Create Self-Hosted Bit Server

Note: We will use port 5000 on our machine.  
_you can replace 5000 with the port you want to use on your host machine_

## Run the server container

1. Run `docker run -it -p 5000:3000 bitcli/bit-server:latest`
2. Browse `http://localhost:5000` and make sure you see the Bit's ui

## Exporting components to bit server

1. Run `bit remote add http://localhost:5000` to add remote scope  
   _you should get a message saying remote-scope was added_
2. Set `"remote-scope"` on your workspace.jsonc as `"defaultScope"`
3. Run `bit export` to export

# Create Private Registry

Note: We will use [Verdaccio](https://verdaccio.org/docs/installation) (private registry) for publishing packages  
_you can use any private/public registry of your choice_

1. Run `docker run -it --name verdaccio -p 4873:4873 verdaccio/verdaccio`
2. Browse `http://localhost:4873` and make sure you see the Verdaccio's ui
3. Run `npm adduser --registry http://localhost:4873/`  
   _add username, password and email for your user_
4. Login to Verdaccio UI using the credentials

# Create Local Bit Components

## Intialize Local Workspace

1. Create a folder  
   `mkdir <foldername>`

2. Initialilze bit workspace  
   `bit init`

3. Set `"<workspace-name>"` on your workspace.jsonc as `"name"`

4. Set `"remote-scope"` on your workspace.jsonc as `"defaultScope"`

5. Add remote scope to your workspace  
   `bit remote add http://localhost:5000`

6. Un-comment `"teambit.react/react": { }`

7. Add the following below the `"teambit.react/react" : {}` line  
   _the properties in packageJson will overwrite your package's metadata_

```
"teambit.pkg/pkg": {
    "packageManagerPublishArgs": ["--access public"],
    "packageJson": {
      "name": "@{scope}/{name}",
      "author": "author-name",
      "private": false,
      "homepage": "http://localhost:4873/@{scope}/{name}",
      "publishConfig": {
        "cache": "cache/.npm",
        "scope": "@{scope}",
        "registry": "http://localhost:4873/"
      }
    }
}
```

## Publish/Export Components

1. Add a "components" folder in root
2. Add a component folder say "button" with four files as shown below

```
components/button
├── button.composition.js
├── button.docs.mdx
├── button.js
└── index.js
```

3. Add component directory for bit to track  
   `bit add components/button --namespace ui`

4. Run bit development server  
   `bit compile`  
   `bit start`

5. Tag the component (this will tag and also publish to registry)  
   `bit tag ui/button`

6. Export the component (this will export to remote scope)  
   `bit export`

## Install/Import Components

1. Vist Verdaccio UI at `http://localhost:4873/`
2. Copy the package install command and run inside project  
   `npm install <package-name>` or `yarn add <package-name>`
3. Import component in your project  
   `import <Component-Name> from "<package-name>"`
4. Use as normal React Component

import React from "react";

function Button({ children, type, ...rest }) {
  const btnType = type || "submit";
  return (
    <div>
      <button type={btnType} {...rest}>
        {children}
      </button>
    </div>
  );
}

export default Button;

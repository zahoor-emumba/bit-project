import React from "react";
import Button from "./button";

export const SimpleButton = (props) => {
  return (
    <Button onClick={() => alert("Simple Button Clicked")}>Show Alert</Button>
  );
};

export const DangerButton = (props) => {
  return (
    <Button style={{ backgroundColor: "red", color: "white" }}>Delete</Button>
  );
};
